import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Inventory management",
      component: () => import("./pages/Index")
    },
    {
      path: "/products",
      name: "products",
      component: () => import("./components/ProductList")
    },
    {
      path: "/add",
      name: "add",
      component: () => import("./components/AddProduct")
    },
    {
      path: "/login",
      name: "login",
      component: () => import("./pages/Login")
    },
    {
      path: "/records",
      name: "records",
      component: () => import("./pages/Records")
    },
  ]
});
