import firebase from "../firebase";

const db = firebase.database().ref("/products");

class ProductService {
  getAll() {
    return db;
  }

  create(product) {
    return db.push(product);
  }

  update(key, value) {
    return db.child(key).update(value);
  }

  delete(key) {
    return db.child(key).remove();
  }

}

export default new ProductService();
